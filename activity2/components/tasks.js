import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, } from "react-native";

export default TaskItem = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.indexContainer}>
                <TouchableOpacity onPress={() => props.deleteTask()}>
                    <Text>Del</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.taskContainer}>
                <Text style={styles.task}>{props.task}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginHorizontal: 20,
    },
    indexContainer: {
        backgroundColor: '#fff',
        borderRadius: 12,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 60,
        marginTop: 20,
    },
    taskContainer: {
        backgroundColor: '#fff',
        borderRadius: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 10,
        minHeight: 50,
        marginTop: 20,
    },
    task: {
        color: 'black',
        width: '90%',
        fontSize: 16,
    },
    delete: {
        marginLeft: 10,
    },
});